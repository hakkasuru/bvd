[DOCUMENTATION]

Note: Windows Script

Flags/Functions:
-a | run script on all '.csv' files in 'input' folder
-f | run script on defined '.csv' file in 'input' folder
-v | prints script version
-h | prints help pages

Usage:
python toHex.py [Options][Values]

Examples:
python toHex.py -a
python toHex.py -v
python toHex.py -h
python toHex.py -f "<filename>.csv"

Recommended Steps for specific file:
1. Copy 'bvd' folder to desktop
2. Open the folder 'bvd' and create folders 'input' and 'output', create '.csv' files and place them in 'input' folder
3. Go to search bar and search for 'cmd'
4. type 'cd C:\<pathto>\bvd'
5. type 'python toHex.py -f "<filename>.csv"'
6. Wait till 'Done :)' shows up

Recommended Steps for all files:
1. Copy 'bvd' folder to desktop
2. Open the folder 'bvd' and create folders 'input' and 'output', create '.csv' files and place them in 'input' folder
3. Go to search bar and search for 'cmd'
4. type 'cd C:\<pathto>\bvd'
5. type 'python toHex.py -a'
6. Wait till 'Done :)' shows up for each file
